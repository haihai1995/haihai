
 function Add(string) {
   let sum = 0;
   const a = string.indexOf("\n");
   const keyWord = string.slice(2, a);
   const b = string.slice(a + 1);
   const strNumber = b.indexOf(keyWord);
   if (strNumber !== -1 && keyWord !== "") {
     const toTal = b.split(keyWord).map(Number);
     const arr = toTal.filter(value => !Number.isNaN(value));
     return arr.reduce((a, b) => a + b);
   }
   if (keyWord === "") {
     const toTal = string.split(/[\W]/).map(Number);
     return toTal.reduce((a, b) => a + b);
   }
   if (strNumber === -1 && keyWord !== "") {
     return sum;
  }
 }
 console.log(Add("//;;\n1;;2;;6;;7"));
